import React, {Fragment, useEffect, useState} from "react";
import cityApi from "../../api/City.api";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CircularProgress from "@material-ui/core/CircularProgress";
import Country from '../country';
import {useDispatch} from "react-redux";
import {loadWeatherData} from "../../redux/actions";
import './index.css';
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";

const theme = createMuiTheme({
    overrides: {
        MuiOutlinedInput: {
            root: {
                "& $notchedOutline": {
                    borderColor: "white"
                },
                "&:hover $notchedOutline": {
                    borderColor: "white"
                },
                "&$focused $notchedOutline": {
                    borderColor: "white"
                },
                "&&& $input": {
                    color: "white"
                }
            }
        }
    }
});

const Index = () => {
    const [options, setOptions] = useState([]);
    const [inputValue, setInputValue] = useState("");
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadWeatherData());
    }, [dispatch]);

    useEffect(() => {
        if (!inputValue || inputValue.length < 4) {
            dispatch(loadWeatherData());
            setOptions([]);
            return undefined;
        }

        (async () => {
            setLoading(true);
            cityApi.search(inputValue)
                .then(cities => {
                    setLoading(false);
                    if (cities.list) {
                        setOptions(cities.list);
                    }
                });
        })();
    }, [inputValue, dispatch]);

    return (
        <Autocomplete
            className='autocomplete'
            onChange={(event, selected) => dispatch(loadWeatherData(selected))}
            getOptionSelected={(option, value) => option.id === value.id}
            getOptionLabel={(option) => option.name}
            onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
            }}
            options={options}
            renderOption={(option) => (
                <Country name={option.name} icon={option.sys.country}/>
            )}
            loading={loading}
            renderInput={(params) => (
                <MuiThemeProvider theme={theme}>
                    <TextField
                        {...params}
                        label="Enter city"
                        variant="outlined"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <Fragment>
                                    {loading ? (
                                        <CircularProgress color="inherit" size={20}/>
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </Fragment>
                            ),
                        }}
                    />
                </MuiThemeProvider>
            )}
        />
    );
};

export default Index;
