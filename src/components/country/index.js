import React, {Fragment} from 'react';

const Country = (props) => {
    const {icon, name} = props;
    return (
        <Fragment>
            <img
                width={40}
                height={30}
                src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${icon.toLowerCase()}.png`}
                alt={icon.toLowerCase()}
            />
            &nbsp;{name}
        </Fragment>
    );
};

export default Country;