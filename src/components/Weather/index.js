import React, {Fragment} from "react";
import {useSelector} from "react-redux";
import GridList from '@material-ui/core/GridList';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import MLabel from '../labels/MLabel';
import SLabel from '../labels/SLabel';
import XLabel from '../labels/XLabel';
import WeatherIcon from '../weatherIcon';
import './index.css';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import moment from "moment";
import makeStyles from "@material-ui/core/styles/makeStyles";

const options = {day: 'numeric', month: 'long', year: 'numeric'};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        height: 180,
        width: 175,
        'background-color': '#ffffff6e'
    },
    control: {
        padding: theme.spacing(2),
    },
    gridList: {
        flexWrap: 'nowrap',
        transform: 'translateZ(0)',
        'overflow-y': 'hidden',
        'padding-top': '10px'
    },
}));

function Weather() {
    const classes = useStyles();
    const city = useSelector(state => state.weatherReducer.city);
    const weather = useSelector(state => state.weatherReducer.weather);
    return (
        city ?
            (<Fragment>
                <Row>
                    <Col xs={12}>
                        <XLabel text={`${city.name}, ${city.sys.country}`}/>
                    </Col>
                    <Col xs={12}>
                        <SLabel
                            text={new Intl.DateTimeFormat('en-US', options).format(new Date(weather.current.dt * 1000))}/>
                    </Col>
                </Row>

                <Container className="temperature" fluid>
                    <Row>
                        <Col xs={6} sm={6} md={6} lg={2} className="center-content">
                            <WeatherIcon icon={`${weather.current.weather[0].icon}@4x.png`}/>
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={3}>
                            <Row className="current-temp">
                                <Col xs={12} className="center-content">
                                    <XLabel text={`${weather.current.temp}°`}/>
                                </Col>
                                <Col xs={12} className="center-content">
                                    <SLabel text={weather.current.weather[0].description}/>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={6}>
                            <Container className="weather-indicator-card">
                                <Row>
                                    <Col xs={6} sm={6} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Humidity"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={`${weather.current.humidity}%`}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={6} sm={6} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Visibility"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={`${weather.current.visibility / 1000}km`}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={12} sm={12} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Pressure"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={`${weather.current.pressure}hPa`}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row style={{'margin-top': '18px'}}>
                                    <Col xs={6} sm={6} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Wind"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={weather.current.wind_speed}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={6} sm={6} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Sunrise"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={new Intl.DateTimeFormat('en-US', {
                                                    hour: '2-digit',
                                                    minute: '2-digit'
                                                }).format(new Date(weather.current.sunrise * 1000))}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={12} sm={12} md={4} lg={4}>
                                        <Row>
                                            <Col xs={12} className="center-content">
                                                <MLabel text="Sunset"/>
                                            </Col>
                                            <Col xs={12} className="center-content">
                                                <MLabel text={new Intl.DateTimeFormat('en-US', {
                                                    hour: '2-digit',
                                                    minute: '2-digit'
                                                }).format(new Date(weather.current.sunset * 1000))}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <GridList className={classes.gridList}>
                                {weather.hourly.map((value) => (
                                    <Grid key={value.dt} item>
                                        <Paper className={classes.paper}>
                                            <Row>
                                                <Col xs={12} className="center-content">
                                                    <SLabel text={moment(new Date(value.dt * 1000)).format("MMMM.Do")}/>
                                                </Col>
                                                <Col xs={12} className="center-content">
                                                    <SLabel text={moment.unix(value.dt).format("hh:mm")}/>
                                                </Col>
                                                <Col xs={12} className="center-content">
                                                    <WeatherIcon icon={`${value.weather[0].icon}@2x.png`}/>
                                                </Col>
                                                <Col xs={12} className="center-content">
                                                    <SLabel text={`${value.temp}°`}/>
                                                </Col>
                                            </Row>

                                        </Paper>
                                    </Grid>
                                ))}
                            </GridList>
                        </Col>
                    </Row>
                </Container>
            </Fragment>)
            : (<Fragment></Fragment>)
    );
}

export default Weather;
