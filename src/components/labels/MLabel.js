import React, {Fragment} from 'react';
import Box from "@material-ui/core/Box";

const XLabel = (props) => {
    const {text} = props;
    return (
        <Fragment>
            <Box component="span"
                 color="background.paper"
                 fontFamily="h6.fontFamily"
                 fontSize={{xs: 'h8.fontSize', sm: 'h7.fontSize', md: 'h5.fontSize'}}
                 px={{xs: 2, sm: 3, md: 4}}>{text}</Box>
        </Fragment>
    );
};

export default XLabel;