import React from 'react';

const WeatherIcon = (props) => {
    const {icon} = props;
    return (
        <img src={`https://openweathermap.org/img/wn/${icon}`}
             alt={icon}/>
    );
};

export default WeatherIcon;