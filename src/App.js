import React from "react";
import "./App.css";
import Search from "./components/Search/index";
import Weather from "./components/Weather";
import Grid from '@material-ui/core/Grid';
import Box from "@material-ui/core/Box";

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <Grid container>
                    <Grid item xs={12} alignItems="left" style={{minHeight: '100px'}}>
                        <Box component="span" display="flex" color="background.paper"
                             fontFamily="h6.fontFamily"
                             style={{'padding': '20px'}}
                             fontSize={{xs: 'h7.fontSize', sm: 'h6.fontSize', md: 'h5.fontSize'}}
                             px={{xs: 2, sm: 3, md: 4}}>Weather forecast Irdeto</Box>
                    </Grid>
                </Grid>
                <Grid container spacing={5}>
                    <Grid item xs={12}>
                        <Search/>
                    </Grid>
                    <Grid item xs={12}>
                        <Weather/>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default App;
