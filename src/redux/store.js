import {createStore, compose, applyMiddleware} from "redux";
import reducers from "./reducers";
import thunk from "redux-thunk";

const initState = {
    weather: [],
    city: null,
};
const enhancers = [
    applyMiddleware(thunk)
];
const composedEnhancers = compose(...enhancers);

export default createStore(
    reducers,
    initState,
    composedEnhancers
);
