import cityApi from "../../api/City.api";

export const loadWeatherData = (city) => (dispatch) => {
    if (!city)
        navigator.geolocation.getCurrentPosition(function (position) {
            cityApi.getCityWeatherDetails(position.coords.longitude, position.coords.latitude)
                .then((data) => {
                    cityApi.searchByLongAndLat(position.coords.longitude, position.coords.latitude)
                        .then(city => {
                            dispatch({
                                type: "LOAD_WEATHER",
                                payload: {
                                    city: city.list[0],
                                    weather: data
                                },
                            });
                        });

                });
        });
    else
        cityApi.getCityWeatherDetails(city.coord.lon, city.coord.lat)
            .then((data) => {
                dispatch({
                    type: "LOAD_WEATHER",
                    payload: {
                        city: city,
                        weather: data,
                    },
                });
            });
};

export const clearWeatherData = () => (dispatch) => {
    dispatch({
        type: "CLEAR_WEATHER",
        payload: {
            city: {},
            weather: [],
        },
    });
};
