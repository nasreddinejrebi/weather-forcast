export default (state = {weather: [], city: null}, action) => {
    switch (action.type) {
        case "LOAD_WEATHER":
            return {
                ...state,
                weather: action.payload.weather,
                city: action.payload.city,
            };
        case "CLEAR_WEATHER":
            return {
                ...state,
                city: null,
                weather: [],
            };
        default:
            return state;
    }
};
