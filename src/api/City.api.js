import axios from 'axios';

export default {
    search(query) {
        return axios.get(`https://openweathermap.org/data/2.5/find?q=${query}&type=like&appid=${process.env.REACT_APP_WEATHER_KEY_PROD}`)
            .then((response) => {
                return response.data;
            });
    },
    searchByLongAndLat(lon, lat) {
        return axios.get(`https://api.openweathermap.org/data/2.5/find?lon=${lon}&lat=${lat}&appid=${process.env.REACT_APP_WEATHER_KEY_LIMITED}`)
            .then((response) => {
                return response.data;
            });
    },
    getCityWeatherDetails(lon, lat) {
        return axios.get(`https://openweathermap.org/data/2.5/onecall?lon=${lon}&lat=${lat}&units=metric&appid=${process.env.REACT_APP_WEATHER_KEY_PROD}`)
            .then((response) => response.data);
    }
};